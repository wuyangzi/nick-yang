<?php

namespace NickYang\Framework\Service;

abstract class AbstractService
{
    /**
     * @var array|string|string[]
     */
    private string|array $model;

    public function __construct()
    {
        $modelClass  = str_replace(['\Service', 'Service'], ['\Model', ''], get_class($this));
        $this->model = $modelClass;//make($modelClass);
    }
}